try:
    # Fix UTF8 output issues on Windows console.
    # Does nothing if package is not installed
    from win_unicode_console import enable
    enable()
except ImportError:
    pass

from pyspark import SparkConf,SparkContext

conf = SparkConf().setMaster("local").setAppName("HW-1 .3")
sc = SparkContext(conf= conf)

input = sc.textFile("1.txt")
words = input.flatMap(lambda x : x.split())
words = words.flatMap(lambda x : x[0])
wordsCount = words.countByValue()

for word, count in wordsCount.items():
    cleanWord = word
    if(cleanWord):
        print(cleanWord + " = " + str(count)) 
